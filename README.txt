Acquia Purge Cloudflare
-----------------------

This module hooks into the Acquia Purge module to further purge URLs from
Cloudflare.


Features
--------
* uses v4 of the Cloudflare API, this is the current and supported version
* will work out the Cloudflare zone ID for you. It is possible to have many
  zones associated with the module (so it is useful for a single database with 2
  different top level domains attached to it)
* integrates with acquia_purge and will purge all domains associated with the
  Acquia environment (so it will work on edit domains etc)
* Sends a single API query per zone ID to do a purge, there is a hard limit of
  2000 API purges a day, so this helps keep you under that limit
* any manual purges done in the Acquia Purge UI, will also purge Cloudflare, as
  acquia_purge_cloudflare hooks into acquia_purge


Requirements
------------

* Your Drupal site hosted on Acquia Cloud using Acquia Purge
* Using Cloudflare as your CDN (either through Acquia Edge CDN or purchasing it
  yourself)
* API access with the ability to purge URLs to the Cloudflare zone in question
  (this requires the #zone:edit permission).


Installation and configuration
------------------------------

1. Install Acquia Purge Cloudflare module as you install a contributed Drupal
   module. See https://drupal.org/documentation/install/modules/themes/modules-7

2. Go to /admin/config/system/expire/cloudflare to configure the
   module.

   Here you will need to paste in your Cloudflare email and API key. You can
   find these at https://www.cloudflare.com/a/account/my-account.

   Once you have saved the admin form, your API keys will be validated, and if
   valid, Cloudflare will automatically receive a purge request for all URLs
   that Acquia Purge is removing.


Debug
-----
There is a variable you can set to enable verbose logging in watchdog for the
API requests, and whether they were successful. I would recommend using this
only temporarily on production.


Credits
-------
Credit goes to Ecuavisa for the original code to which was used as the basis for
this module.


Caveats
-------
This module is a very simple implementation of the Cloudflare purging API, and
will only purge by URL (and not by tag). There is work to consolidate this
module's functionality into the Acquia Purge module, but this is some time away.
At such a point when Acquia Purge does what this module does, then this module
will be considered deprecated.
