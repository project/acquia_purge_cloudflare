<?php

/**
 * @file
 * Admin page callbacks for the acquia_purge_cloudflare module.
 */

/**
 * Menu callback; Provide the administration overview page.
 */
function acquia_purge_cloudflare_settings() {
  $form['acquia_purge_cloudflare_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Your Cloudflare email'),
    '#description' => t('Your login email address for Cloudflare.'),
    '#default_value' => variable_get('acquia_purge_cloudflare_email', ''),
    '#required' => TRUE,
  );

  $acquia_purge_cloudflare_apikey = variable_get('acquia_purge_cloudflare_apikey', '');
  $form['acquia_purge_cloudflare_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('Your Cloudflare API key'),
    '#description' => t('You can get your API key from <a href="!url">Cloudflare</a>. If you wish to delete this key, type the word <code>DELETE</code> in the field.', array(
      '!url' => ACQUIA_PURGE_CLOUDFLARE_ACCOUNT_PAGE
    )),
    '#attributes' => array(
      'placeholder' => empty($acquia_purge_cloudflare_apikey) ? '' : substr($acquia_purge_cloudflare_apikey, 0, 4) . ' ... (full key not displayed)',
    )
  );
  $form['acquia_purge_cloudflare_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debug logging'),
    '#description' => t('If enabled, then API responses are logged in Drupal watchdog. It is recommended to not leave this enabled when running in production.'),
    '#default_value' => variable_get('acquia_purge_cloudflare_debug', 0),
  );

  return system_settings_form($form);
}

/**
 * Implements additional validation logic for
 * acquia_purge_cloudflare_settings().
 */
function acquia_purge_cloudflare_settings_validate($form, &$form_state) {
  $email = trim($form_state['values']['acquia_purge_cloudflare_email']);
  $apikey = trim($form_state['values']['acquia_purge_cloudflare_apikey']);

  // If empty, just keep the existing API key.
  if (empty($apikey)){
    $apikey = variable_get('acquia_purge_cloudflare_apikey', '');
    $form_state['values']['acquia_purge_cloudflare_apikey'] = $apikey;
  }
  // Users can also delete the API key.
  elseif ($apikey === 'DELETE'){
    $form_state['values']['acquia_purge_cloudflare_apikey'] = NULL;
    return;
  }

  $api_url = ACQUIA_PURGE_CLOUDFLARE_API_ENDPOINT . "zones";
  $result = drupal_http_request($api_url, array(
    'method' => 'GET',
    'headers' => array(
      'X-Auth-Email' => $email,
      'X-Auth-Key' => $apikey,
      'Content-Type' => 'application/json'
    )
  ));

  // API request was successful.
  if ($result->code == 200) {
    $data = drupal_json_decode($result->data);
    $form_state['values']['acquia_purge_cloudflare_verified'] = TRUE;
    drupal_set_message(format_plural($data['result_info']['total_count'], 'Connection to Cloudflare API successful, you have access to 1 zone.', 'Connection to Cloudflare API successful, you have access to @count zones.'));
  }
  else {
    $form_state['values']['acquia_purge_cloudflare_verified'] = FALSE;
    form_set_error('acquia_purge_cloudflare_email', t('Incorrect email and API key combination'));
    form_set_error('acquia_purge_cloudflare_apikey');
  }
}
