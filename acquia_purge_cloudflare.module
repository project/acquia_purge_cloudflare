<?php

/**
 * @file
 * Acquia Purge, Top-notch Cloudflare purging on Acquia Cloud!
 */

define('ACQUIA_PURGE_CLOUDFLARE_ACCOUNT_PAGE', 'https://www.cloudflare.com/a/account/my-account');
define('ACQUIA_PURGE_CLOUDFLARE_API_ENDPOINT', 'https://api.cloudflare.com/client/v4/');
define('ACQUIA_PURGE_CLOUDFLARE_MAX_URLS_PER_PURGE', 30);

/**
 * Implements hook_help().
 */
function acquia_purge_cloudflare_help($path, $arg) {
  switch ($path) {
    case 'admin/help#acquia_purge_cloudflare':
      $readme_file = dirname(__FILE__) . '/README.txt';
      if (file_exists($readme_file)) {
        $readme = file_get_contents($readme_file);
      }
      if (!isset($readme)) {
        return '';
      }
      // Markdown is optionally supported if you already have it installed.
      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];

        if (function_exists($info['process callback'])) {
          $function = $info['process callback'];
          $output = filter_xss_admin($function($readme, NULL));
        }
        else {
          $output = '<pre>' . check_plain($readme) . '</pre>';
        }
      }
      // Else you get a plain version of the README file.
      else {
        $output = '<pre>' . check_plain($readme) . '</pre>';
      }

      return $output;
  }
}

/**
 * Implements hook_permission().
 */
function acquia_purge_cloudflare_permission() {
  return array(
    'administer acquia purge cloudflare' => array(
      'title' => t('Administer Acquia Purge CloudFlare'),
      'description' => t('Allows users to administer Acquia Purge CloudFlare settings.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function acquia_purge_cloudflare_menu() {
  $items['admin/config/system/expire/cloudflare'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => 'Cloudflare',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('acquia_purge_cloudflare_settings'),
    'access arguments' => array('administer acquia purge cloudflare'),
    'file' => 'acquia_purge_cloudflare.admin.inc',
  );

  return $items;
}

/**
 * Determines whether you can send API queries to Cloudflare.
 *
 * @return bool
 */
function acquia_purge_cloudflare_can_send_api_queries() {
  $email = variable_get('acquia_purge_cloudflare_email', '');
  $apikey = variable_get('acquia_purge_cloudflare_apikey', '');
  $verified = variable_get('acquia_purge_cloudflare_verified', FALSE);
  return ($verified && !empty($email) && !empty($apikey));
}

/**
 * Implements hook_acquia_purge_purge_success().
 *
 * This hook is fired after the queue has been successfully processed, so
 * Varnish cache will already have had it's cache cleared.
 *
 * @param string[] $paths
 *   Non-associative array of string values representing the purged paths.
 */
function acquia_purge_cloudflare_acquia_purge_purge_success($paths) {
  global $base_url;

  // Get a list of domains that this site is configured to use in Acquia
  // Insight.
  $domains = _acquia_purge_get_domains();
  if (empty($domains)) {
    $domain = str_replace(array('http://', 'https://'), '', $base_url);
    $domains = array($domain);
  }

  foreach ($domains as $id => $domain) {
    $zone_id = acquia_purge_cloudflare_get_zone_id($domain);

    // Normalise URLs to be absolute URLs.
    foreach ($paths as $index => $path) {
      if (strpos($path, 'http') !== 0) {
        // URL is not absolute, use domain to create.
        $paths[$index] = url($path, array(
          'absolute' => true
        ));
      }
    }

    // De-duplicate the URLs.
    $paths = array_unique($paths);

    // Bulk send URLs to Cloudflare to be purged.
    if (acquia_purge_cloudflare_purge_urls($paths, $zone_id)) {
      watchdog('acquia_purge_cloudflare', 'URLs: %paths were cleared from Cloudflare using zone ID %zone_id.', array(
        '%paths' => implode(', ', $paths),
        '%zone_id' => $zone_id,
      ), WATCHDOG_INFO);
    }
  }
}

/**
 * Attempt to get the zone ID for the TLD in question. This will only work if
 * your Cloudflare account has access to the TLD in question.
 *
 * @param $domain
 *   The domain of the site, provided by Acquia Purge.
 * @param bool $use_cache
 * @return mixed|string
 */
function acquia_purge_cloudflare_get_zone_id($domain) {
  $email = variable_get('acquia_purge_cloudflare_email', '');
  $apikey = variable_get('acquia_purge_cloudflare_apikey', '');
  $debug = (bool) variable_get('acquia_purge_cloudflare_debug', 0);
  $zone_id = NULL;

  if (!acquia_purge_cloudflare_can_send_api_queries()) {
    return FALSE;
  }

  // Remove the www prefix as Cloudflare works on a TLD level only.
  foreach (array('www.', 'dev.', 'test.', 'edit.') as $prefix) {
    if (strpos($domain, $prefix) === 0) {
      $domain = str_replace($prefix, '', $domain);
    }
  }

  if ($debug) {
    watchdog('acquia_purge_cloudflare', 'Domain being used for zone ID lookup: %domain.', array(
      '%domain' => $domain,
    ), WATCHDOG_DEBUG);
  }

  // Attempt to lookup the existing zone from cache.
  $cid = 'acquia_purge_cloudflare_zone_id_' . str_replace('.', '_', $domain);
  $zone_id = variable_get($cid, NULL);
  if (!empty($zone_id)) {
    return $zone_id;
  }

  $api_url = ACQUIA_PURGE_CLOUDFLARE_API_ENDPOINT . "zones?name={$domain}&status=active";
  $result = drupal_http_request($api_url, array(
    'method' => 'GET',
    'headers' => array(
      'X-Auth-Email' => $email,
      'X-Auth-Key' => $apikey,
      'Content-Type' => 'application/json'
    )
  ));

  // API request was successful.
  if ($result->code == 200) {
    $json = json_decode($result->data);
    if ($debug) {
      watchdog('acquia_purge_cloudflare', 'JSON from API: <pre>@json</pre>', array(
        '@json' => print_r($json, TRUE),
      ), WATCHDOG_DEBUG);
    }

    foreach($json->result as $item) {
      if ($item->id) {
        $zone_id = $item->id;
        break;
      }
    }

    // Caches the entry to avoid going again.
    // @TODO better validation of zone ID.
    if (!empty($zone_id)) {
      variable_set($cid, $zone_id);
      if ($debug) {
        watchdog('acquia_purge_cloudflare', 'Zone ID set to: %zone_id', array(
          '%zone_id' => $zone_id,
        ), WATCHDOG_DEBUG);
      }
    }
  }
  else {
    if ($debug) {
      watchdog('acquia_purge_cloudflare', 'API result: <pre>@result</pre>', array(
        '@result' => print_r($result, TRUE),
      ), WATCHDOG_DEBUG);
    }
  }

  return $zone_id;
}

/**
 * Send a purge request to Cloudflare. Cloudflare permission needed: #zone:edit.
 *
 * @see https://api.cloudflare.com/#zone-purge-individual-files-by-url-and-cache-tags
 *
 * @param $paths
 * @param $zone_id
 * @return bool
 */
function acquia_purge_cloudflare_purge_urls($paths, $zone_id) {
  $email = variable_get('acquia_purge_cloudflare_email', '');
  $apikey = variable_get('acquia_purge_cloudflare_apikey', '');
  $debug = (bool) variable_get('acquia_purge_cloudflare_debug', 0);

  if (!acquia_purge_cloudflare_can_send_api_queries()) {
    return FALSE;
  }

  $api_url = ACQUIA_PURGE_CLOUDFLARE_API_ENDPOINT . "zones/{$zone_id}/purge_cache";

  if ($debug) {
    watchdog('acquia_purge_cloudflare', 'API URL: %api_url', array(
      '%api_url' => $api_url,
    ), WATCHDOG_DEBUG);
  }

  // We need to chunk the API requests to Cloudflare, to not exceed their API
  // limits.
  $success = TRUE;
  foreach (array_chunk($paths, ACQUIA_PURGE_CLOUDFLARE_MAX_URLS_PER_PURGE) as $chunk) {
    $result = drupal_http_request($api_url, array(
      'method' => 'DELETE',
      'headers' => array(
        'X-Auth-Email' => $email,
        'X-Auth-Key' => $apikey,
        'Content-Type' => 'application/json'
      ),
      'data'=> '{"files":["' . implode('", "', $chunk) . '"]}',
    ));

    // Open the file using the HTTP headers set above.
    if ($result->code == 200) {
      $data = json_decode($result->data);

      if ($debug) {
        watchdog('acquia_purge_cloudflare', 'API result: <pre>@data</pre>', array(
          '@data' => print_r($data, TRUE),
        ), WATCHDOG_DEBUG);
      }

      if (!$data->success) {
        watchdog('acquia_purge_cloudflare', "Couldn't clear URL cache for %paths. Reason: <pre>%reason</pre>", array(
          '%paths' => implode(', ', $chunk),
          '%reason' => print_r($result->errors, TRUE),
        ), WATCHDOG_ERROR);
        return FALSE;
      }
    }
  }

  return TRUE;
}
